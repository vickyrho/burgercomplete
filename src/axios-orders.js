import axios from 'axios' ;

const instance = axios.create({
    baseURL: 'https://burger-builder-vickyrho.firebaseio.com/'
});

export default instance ;