import React, {Component} from 'react' ;
import Aux from '../Aux/Aux';
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component {

    state = {
        showsideDrawer: false
    }

    sideDrawerClosedHandler = () => {
        this.setState({showsideDrawer: false});
    }

    _toggleSideDrawer = () => {
        this.setState((prevState) => {
            return  { showsideDrawer : !prevState.showsideDrawer };
        });
    }


    render () {
        return (
            <Aux>
                <div>
                    <Toolbar toggleSideDrawer={this._toggleSideDrawer} />
                    <SideDrawer open={this.state.showsideDrawer} closed={this.sideDrawerClosedHandler}/>
                </div>
                <main className={classes.content}>
                    { this.props.children }
                </main>
            </Aux>
        );
    };
};

export default Layout ;