import React, { Component } from 'react';
import Aux from '../../hoc/Aux/Aux';
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BurgerControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummay';
import axios from '../../axios-orders' ;
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actionTypes from '../../store/actions';
import { connect } from 'react-redux' ;


class BurgerBuilder extends Component {

    state = {
      purchasing: false,
      loading: false,
      error: false
    };

    componentDidMount () {
        // good place to fetch data from server and place to initialize server related data
        axios.get('https://burger-builder-vickyrho.firebaseio.com/ingredients.json').then(response => {
            this.setState({ ingredients: response.data });
        }).catch(error => {
            this.setState({ error : true});
        });
    }

    // addIngredientHandler = (type) => {
    //     const oldCount = this.state.ingredients[type];
    //     const updatedCount = oldCount + 1;
    //     const updatedIngredients = {
    //         ...this.state.ingredients
    //     };
    //     updatedIngredients[type] = updatedCount;
    //     const priceAddition = INGREDIENT_PRICES[type];
    //     const newPrice = this.state.totalPrice + priceAddition ;
    //     this.setState({ingredients: updatedIngredients,totalPrice: newPrice})
    //     this.updatePurchase(updatedIngredients);

    // };

    updatePurchase = (ingredients) => {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey]
            })
            .reduce((sum,el)=>{
                return sum + el ;
            },0);
        return sum > 0 ;
    };

    // removeIngredientHandler = (type) => {
    //     const oldCount = this.state.ingredients[type];
    //     if( oldCount <= 0 )
    //         return ;
    //     const updatedCount = oldCount - 1;
    //     const updatedIngredients = {
    //         ...this.state.ingredients
    //     };
    //     updatedIngredients[type] = updatedCount;
    //     const priceAddition = INGREDIENT_PRICES[type];
    //     const newPrice = this.state.totalPrice - priceAddition ;
    //     this.setState({ingredients: updatedIngredients,totalPrice: newPrice})
    //     this.updatePurchase(updatedIngredients);

    // };

    purchaseHandler = () => {
        console.log('hello');
        this.setState({purchasing:true});
    };

    cancelPurchaseHandler = () => {
      this.setState({purchasing:false});
    };

    continuePurchaseHandler = () => {
    //   alert("purchased");
    //   this.setState({ loading: true });
    //   const order = {
    //       ingredients: this.state.ingredients,
    //       price: this.state.totalPrice,
    //       customer: {
    //           name: 'Vignesh',
    //           address: {
    //               street: '1st street',
    //               zipCode: '1234',
    //               country: 'India'
    //           },
    //           email: 'vignesh@gmail.com',
    //         },
    //         deliveryMethod: 'premium'
    //       }

    //   axios.post('/orders.json',order).then(response => { 
    //     this.setState({ loading: false, purchasing: false });
    // }).catch(error => {
    //     this.setState({ loading: false, purchasing: false });
    //   })
    // const queryParams = [];
    // for(let i in this.state.ingredients) {
    //     queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
    // }
    // const price = this.state.totalPrice ;
    // queryParams.push('price='+price);
    // const queryString = queryParams.join('&');
    this.props.history.push('/checkout');
    };

    render(){

        const disabledInfo = {
            ...this.props.ings
        };

        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] <= 0 ;
        }

        let orderSummary = null ;

        let burger = this.state.error ? <p>ingredients are not loaded</p> : <Spinner /> ;

        if(this.state.ingredients){
            burger = (<Aux>
                <Burger ingredients={this.props.ings}/>
                    <BuildControls
                        price = {this.props.price}
                        ingredientAdded = {this.props.onIngredientsAdded}
                        ingredientRemoved = {this.props.onIngredientsRemoved}
                        disabled = {disabledInfo}
                        purchasable = {this.updatePurchase(this.props.ings)}
                        ordered={this.purchaseHandler}
                    />
            </Aux>);

            orderSummary = 
                <OrderSummary 
                    ingredients={this.props.ings} 
                    purchaseCancel={this.cancelPurchaseHandler}
                    purchaseContinue={this.continuePurchaseHandler} 
                    price = {this.props.price} 
                />

        }

        if(this.state.loading) {
            orderSummary = <Spinner />
        }

        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed = {this.cancelPurchaseHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientsAdded : (ingName) => dispatch({type: actionTypes.ADD_INGREDIENT, ingredientName: ingName}),
        onIngredientsRemoved: (ingName) => dispatch({type: actionTypes.REMOVE_INGREDIENT, ingredientName: ingName})
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));