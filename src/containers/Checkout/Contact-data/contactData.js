import React, {Component} from 'react' ;
import Button from '../../../components/UI/Button/Button';
import classes from './contactData.css';
import axios from '../../../axios-orders' ;
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Forms/Input/Input';
import { connect } from 'react-redux' ;

class ContactData extends Component {

    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 25
                },
                valid: false,
                touched: false
                // shouldValidate: true
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your Email'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 25
                },
                valid: false,
                touched: false
                // shouldValidate: true
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your street'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 25
                },
                valid: false,
                touched: false
                // shouldValidate: true
            },
            postalCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your zipcode'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 25
                },
                valid: false,
                touched: false
                // shouldValidate: true
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your country'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 25
                },
                valid: false,
                touched: false
                // shouldValidate: true
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {
                            value: 'fastest', displayValue: 'Fastest'
                        },
                        {
                            value: 'normal', displayValue: 'Normal'
                        }
                    ]
                },
                value: 'fastest',
                valid: true,
                validation : {

                }
                // shouldValidate: false
            }
        },
        isFormValid: false,
        loading: false
    }

    orderHandler = (e) => {
      e.preventDefault();
      this.setState({ loading: true });
      const formData = {};
      for(let item in this.state.orderForm) {
          formData[item] = this.state.orderForm[item].value;
      }
      const order = {
          ingredients: this.props.ings,
          price: this.props.price,
          orderData: formData
        }
      axios.post('/orders.json',order).then(response => { 
        this.setState({ loading: false });
        this.props.history.push('/');
    }).catch(error => {
        this.setState({ loading: false });
      })
    }

    InputChangeHandler = (e, inputElement) => {
        const updatedOrderState = {
            ...this.state.orderForm
        }
        const orderElement = { ...updatedOrderState[inputElement] };
        orderElement.value = e.target.value;
        orderElement.valid = this.checkValidity(orderElement.value, orderElement.validation);
        orderElement.touched = true;
        updatedOrderState[inputElement] = orderElement;
        let formIsValid = true ;
        for (let item in updatedOrderState) {
            formIsValid = updatedOrderState[item].valid && formIsValid;
        }
        console.log(formIsValid);
        this.setState({
            orderForm: updatedOrderState, isFormValid: formIsValid
        });
    }

    checkValidity(value, rules) {
        let isValid = false;
        if (rules.required) {
            isValid = value.trim() !== '';
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        return isValid ; 
    }

    render() {
        const formElementsArray = [];
        for(let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }
        let form = null;
        if(this.state.loading)
            form = (<Spinner />)
        else 
            form = (
                <form onSubmit={this.orderHandler} >
                    {
                        formElementsArray.map(formElement => (
                            <Input
                                key={formElement.id} 
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                changed={(event) => this.InputChangeHandler(event,formElement.id)}
                                touched={formElement.config.touched}
                                shouldValidate={formElement.config.validation}
                            />
                        ))
                    }
                    <Button disabled={!this.state.isFormValid} buttonType="Success">Submit</Button>
                </form>
            );
        return (
            <div className={classes.contactData}>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    }
}

export default connect(mapStateToProps)(ContactData);