import React, { Component } from 'react' ;
import CheckoutSummary from '../../components/Order/CheckoutSummary/checkoutSummary';
import ContactData from './Contact-data/contactData';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

class Checkout extends Component {
   
    checkoutContinueHandler = () => {
        console.log(this.props);
        this.props.history.replace("/checkout/contact-data");
    }

    checkoutCancelHandler = () => {
        this.props.history.goBack();
    }

    render(){
        return (
            <div>
                <CheckoutSummary 
                    ingredients={this.props.ings} 
                    checkoutContinue={this.checkoutContinueHandler} 
                    checkoutCancel={this.checkoutCancelHandler} />
                <Route 
                    path={this.props.match.path + '/contact-data'} 
                    // render={ (props) => (<ContactData ingredients={this.props.ings} price={this.props.price} {...props} />) } 
                    component={ContactData}
                ></Route>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return  {
        ings: state.ingredients,
    }
}

export default connect(mapStateToProps)(Checkout);