import React from 'react' ;
import classes from './Spinner.css'
const spinner = () => (
    <div className={classes.Loader} style={ { backgroundColor: 'blue' } } >Loading...</div>
);

export default spinner ;