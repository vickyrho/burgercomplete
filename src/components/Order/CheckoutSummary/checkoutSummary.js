import React from 'react';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import classes from './checkoutSummary.css';

const checkoutSummary = (props) => {
    return (
        <div className={classes.checkoutSummary} >
            <div style={{width:'100%' , margin:'auto'}}>
                <Burger ingredients={props.ingredients} />
            </div>
            <Button 
                buttonType="Success"
                clicked={props.checkoutContinue}>Continue
            </Button>
            <Button 
                buttonType="Danger"
                clicked={props.checkoutCancel} >Cancel
            </Button>
        </div>
    );
}

export default checkoutSummary;