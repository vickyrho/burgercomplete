import React from 'react' ;
import classes from './order.css';

const order = (props) => {

    const ingredients = [];

    for(let item in props.ingredients) {
        ingredients.push({name: item, quantity: props.ingredients[item]});
    }

    const ingredientsOutput = ingredients.map(ig => (
    <span>{ig.name} {ig.quantity}</span>
    ));
    
    return (
        <div className={classes.order}>
            {ingredientsOutput}
            <p>total price: {props.price.toFixed(2)}</p>
        </div>
    );
}

export default order;