import React , {Component } from 'react';
import Order from './Order/order';
import axios from '../../axios-orders';
// import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';

class Orders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            loading: true
        }
    }

    componentWillMount(){
        axios.get("./orders.json")
            .then(res => {
                console.log(res.data);
                const fetchedOrders = [];
                for(let i in res.data) {
                    fetchedOrders.push({
                        ...res.data[i],
                        id: i
                    })
                }
                this.setState({loading: false, orders: fetchedOrders});
            })
            .catch(err => {
                console.log(err);
                this.setState({loading: false});
            });

    }

    render() {
        return (
            <div>
                {this.state.orders.map(order => (
                    <Order 
                        id={order.id} 
                        price={+order.price}
                        ingredients={order.ingredients}
                    />
                ))}
            </div>
        );
    }

}

export default Orders ;