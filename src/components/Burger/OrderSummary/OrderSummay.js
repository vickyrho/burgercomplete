import React, { Component } from 'react';

import Aux from '../../../hoc/Aux/Aux'
import Button from '../../UI/Button/Button'

class OrderSummary extends Component {

    componentWillUpdate() {
        console.log('[order summary update]');
    }

    render (){
        const ingredientSummary = Object.keys(this.props.ingredients)
        .map(igKey => {
            return <li key={igKey}>
                    <span style={{textTransform:'capitalize'}}>
                        {igKey}
                    </span> : {this.props.ingredients[igKey]}
                    </li>
        });
        return (
            <Aux>
                <h3>Your Order</h3>
                <p>Burger is ready</p>
                <ul>{ingredientSummary}</ul>
                <p><strong>Total price : {this.props.price.toFixed(2)} </strong></p>
                <p>Continue to summary</p>
                <Button buttonType="Danger" clicked={this.props.purchaseCancel} >Cancel</Button>
                <Button buttonType="Success" clicked={this.props.purchaseContinue} >Continue</Button>
            </Aux>
        );
    }
}

export default OrderSummary;